/**
 * Asynchronous Example: 
 * A waiter will take orders and bring it to the kitchen and then
 * begin taking the next order until the food is ready. 
 * 
 * Synchronous Example: 
 * The waiter will take the order and then go to the kitchen and wait
 * till the food is ready before getting the next order. 
 */

console.log('Before');

// Ayschronous Function (non-blocking function - schedules a task)
// DO NOT MEAN CONCURRENT OR MULTITHREADED - it is still single-
// threaded. 
setTimeout(() => {
  console.log('Reading a user from a database');
}, 2000);

console.log('After');

