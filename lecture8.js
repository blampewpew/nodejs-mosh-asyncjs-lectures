/**
 * Promise API:
 */

 // Simulate a fulfilled promise (good for unit testing)
 const p = Promise.resolve({ id: 1}); // Returns promise that has been resolved
 p.then(result => console.log(result));

  // Simulate a rejected promise (good for unit testing)
const p = Promise.reject(new Error('reason for rejecting...')); // Returns promise that has been resolved
 p.catch(error => console.log(error));