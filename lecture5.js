/**
 * Callback Hell Solution:
 */

console.log('Before');
getUser(1, getRepositories); 
console.log('After');

function getRepositories(user) {
  console.log(user);
  getRepositories(user.gitHubUsername, getCommits);
}

function getCommits(repositories) {
  console.log(repositories);
  getCommits(repositories, displayCommits);
}

function displayCommits(commits) {
  console.log(commits);
}

function getUser(id, callback) {
  setTimeout(() => {
    console.log('Reading a user from a database...');
    callback({ id: id, gitHubUsername: 'mosh' });
  }, 2000);
}

// Convert this to an async function. 
function getRepositories(username, callback) {
  setTimeout(() => {
    console.log('Retrieving repositories...');
    callback(['repo1', 'repo2', 'repo3']);
  }, 2000);

}