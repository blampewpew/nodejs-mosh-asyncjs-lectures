/**
 * Promises (Object):
 * Holds the eventual result of an async operation.
 * 
 * It will start off as a "Pending" which either becomes:
 *  - Fulfilled
 *  - Rejected
 * 
 * It's best the replace all callback functions with promises.
 */

 const p = new Promise(function(resolve, reject) {
  // Kick off some async work
  setTimeout(() => {
    resolve(1); // Produces 1 as result or a database object (FULFILLED)
    reject(new Error('message')); // (REJECTED)
  }, 2000);
});

// Consume this promise
p.then(result => console.log('Result', result))
  .catch(err => console.log('Error', err.message));