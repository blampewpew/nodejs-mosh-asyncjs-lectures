/**
 * Callback Hell:
 */

// Asynchronous - Harder to read due to this nesting structure.
console.log('Before');
getUser(1, (user) => {
  // Get the Repositories
  getRepositories(user.gitHubUsername, (repositories) => {
    getCommits(repo, (commits) => () {
      // CALLBACK HELL
    });
  });
}); 
console.log('After');

// Synchronous - A lot easier to read.
console.log('Before');
const user = getUser(1);
const repos = getRepositories(user.gitHubUsername);
const commits = getCommits(repos[0]);
console.log('After');


function getUser(id, callback) {
  setTimeout(() => {
    console.log('Reading a user from a database...');
    callback({ id: id, gitHubUsername: 'mosh' });
  }, 2000);
}

// Convert this to an async function. 
function getRepositories(username, callback) {
  setTimeout(() => {
    console.log('Retrieving repositories...');
    callback(['repo1', 'repo2', 'repo3']);
  }, 2000);

}