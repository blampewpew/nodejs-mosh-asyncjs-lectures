/**
 * Callbacks: 
 * Uses a callback function which will be called when the aysnc
 * function is ready.
 */

console.log('Before');
getUser(1, (user) => {
  console.log('User', user);

  // Get the Repositories
  getRepositories(user.gitHubUsername, (repositories) => {
    console.log(repositories);
  });
}); 
console.log('After');


function getUser(id, callback) {
  setTimeout(() => {
    console.log('Reading a user from a database...');
    callback({ id: id, gitHubUsername: 'mosh' });
  }, 2000);
}

// Convert this to an async function. 
function getRepositories(username, callback) {
  setTimeout(() => {
    console.log('Retrieving repositories...');
    callback(['repo1', 'repo2', 'repo3']);
  }, 2000);

}