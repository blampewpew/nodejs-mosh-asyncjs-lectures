/**
 * Parallel Promises:
 * Example is using promises that call simulatenous apis
 * like calling Twitter or Facebook at the same time. 
 */

//Simulating FACEBOOK API
const p1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('Async Operation 1...');
    resolve(1);
    // reject(new Error('something has failed...'));
  }, 2000);
});

//Simulating TWITTER API
const p2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('Async Operation 2...');
    resolve(2);
  }, 2000);
});

// Promise fulfilled when all promises are resolved.
// Promise.all([p1, p2])
//   .then(result => console.log(result))
//   .catch(err => console.log('Error', error.message));

// Promise as soon as one of the promise is resolved (the first one)
Promise.race([p1, p2])
  .then(result => console.log(result))
  .catch(err => console.log('Error', error.message));