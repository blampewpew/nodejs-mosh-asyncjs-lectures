console.log('Before');

/**
 * Cannot use const. The variable will be undefined.
 * Retrieving data from the database may take some time. 
 */
const user = getUser(1);
console.log(user); 
console.log('After');

/**
 * 3 ways to handle:
 * 1. Callbacks
 * 2. Promises
 * 3. Async/await
 */

function getUser(id) {
  setTimeout(() => {
    console.log('Reading a user from a database...');
    return { id: id, gitHubUsername: 'mosh' };
  }, 2000);
}

